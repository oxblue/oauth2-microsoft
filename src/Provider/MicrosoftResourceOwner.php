<?php namespace OxBlue\OAuth2\Client\Provider;

use League\OAuth2\Client\Provider\ResourceOwnerInterface;

class MicrosoftResourceOwner implements ResourceOwnerInterface
{
    /**
     * Raw response
     *
     * @var array
     */
    protected $response;

    /**
     * Creates new resource owner.
     *
     * @param array  $response
     */
    public function __construct(array $response = array())
    {
        $this->response = $response;
    }

    /**
     * Get resource owner id
     *
     * @return string|null
     */
    public function getId()
    {
        return $this->response['id'] ?: null;
    }

    /**
     * Get user principal name
     *
     * @return string|null
     */
    public function getUserPrincipalName()
    {
        return $this->response['userPrincipalName'] ?: null;
    }


    /**
     * Get user principal name
     *
     * @return string|null
     */
    public function getDisplayName()
    {
        return $this->response['displayName'] ?: null;
    }


    /**
     * Get user principal name
     *
     * @return string|null
     */
    public function getSurname()
    {
        return $this->response['surname'] ?: null;
    }


    /**
     * Get user principal name
     *
     * @return string|null
     */
    public function getGivenName()
    {
        return $this->response['givenName'] ?: null;
    }

    /**
     * Return all of the owner details available as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return $this->response;
    }
}
